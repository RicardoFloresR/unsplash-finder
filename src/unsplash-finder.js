import {LitElement, html} from 'lit-element';
import 'unsplash-dm/src/unsplash-dm.js';
import 'unsplash-img-detail/src/unsplash-img-detail.js';
import 'unsplash-imgs-results/src/unsplash-imgs-results.js';
import 'unsplash-input-data/src/unsplash-input-data.js';

export class UnsplashFinder extends LitElement {
	static get is() {
		return 'unsplash-finder';
	}

	static get properties() {
		return{
			imgsResults: {
				type: Object,
				attribute: 'imgs-results'
			}
		};
	}

	constructor() {
		super();
	}

	render() {
		return html`
		<unsplash-input-data @unsplash-input-send-sata="${this._startSearch}"></unsplash-input-data>
		${this.imgsResults ? html`<unsplash-imgs-results content="${JSON.stringify(this.imgsResults)}"></unsplash-imgs-results>` : ''}
		<unsplash-dm id="unsplash-dm" @unsplash-dm-send-data="${this._catchData}"></unsplash-dm>
		`;
	}
	_startSearch(e) {
		this.shadowRoot.getElementById('unsplash-dm').setAttribute('search-key', e.detail);
	}
	_catchData(e) {
		this.setAttribute('imgs-results', JSON.stringify(e.detail));
		debugger;
	}


}

customElements.define(UnsplashFinder.is, UnsplashFinder);